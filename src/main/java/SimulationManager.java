import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;


public class SimulationManager implements Runnable {

    private int nrOfClients;
    private int nrOfServers;
    private int maxSimulation;
    private int minArrival;
    private int maxArrival;
    private int minService;
    private int maxService;
    private List<Server> servers;
    private List<Thread> threads;
    private List<Task> generatedTasks;
    private TextArea textArea;
    private float averageServiceTime;
    private int peakHour;
    private int maxClients;
    private float averageWaitingTime;


    public SimulationManager(int nrOfClients,int nrOfServers,int maxSimulation,int minArrival,
                             int maxArrival, int minService,int maxService,TextArea textArea){
        this.nrOfClients = nrOfClients;
        this.nrOfServers = nrOfServers;
        this.maxSimulation = maxSimulation;
        this.minArrival = minArrival;
        this.maxArrival = maxArrival;
        this.minService = minService;
        this.maxService = maxService;
        this.textArea = textArea;
        servers = new ArrayList<>();
        threads = new ArrayList<>();
        averageServiceTime = 0;
        peakHour = 0;
        maxClients = 0;
        averageWaitingTime = 0;
        for (int i=0;i<nrOfServers;i++){
            Server server = new Server("Thread" + i);
            servers.add(server);
            Thread thread = new Thread(server);
            thread.setName("Thread " + i);
            threads.add(thread);
            thread.start();
        }
        generateNRandomTasks();
    }
    private void generateNRandomTasks(){
        generatedTasks = new ArrayList<>();
        for(int i=0;i<nrOfClients;i++){
            Task newTask = new Task();
            Random random = new Random();
            newTask.setId(i+1);
            newTask.setArrivalTime(random.nextInt(maxArrival-minArrival)+minArrival);
            newTask.setProcessingTime(random.nextInt(maxService-minService)+minService);
            generatedTasks.add(newTask);
            Collections.sort(generatedTasks,new SortTasks());
        }
    }
    public void prepareServers(int currentTime){
        for(int i=0;i< generatedTasks.size();i++){
            if(generatedTasks.get(i).getArrivalTime() == currentTime){
                int minPeriod = Integer.MAX_VALUE;
                int k = 0;
                for(int j = 0;j < servers.size();j++){
                    if(servers.get(j).getWaitingPeriod().get() < minPeriod){
                        minPeriod = servers.get(j).getWaitingPeriod().get();
                        k = j;
                    }
                }
                averageServiceTime+=generatedTasks.get(i).getProcessingTime();
                servers.get(k).addTask(generatedTasks.get(i));

                generatedTasks.remove(generatedTasks.get(i));
                i--;
            }
        }
    }
    public void computePeakHour(int currentTime){
        int nrClients = 0;
        for(int i = 0;i<servers.size();i++){
            for(int j=0;j<servers.get(i).getTasks().size();j++){
                nrClients++;
            }
        }
        if(nrClients > maxClients){
            maxClients = nrClients;
            peakHour = currentTime;
        }
    }
    public void computeAverageWaiting(){
        for(int i = 0;i<servers.size();i++){
            if(servers.get(i).getTasks().size() > 1){
                for(int j=1;j<servers.get(i).getTasks().size();j++){
                    for(int k=j-1;k>=0;k--){
                        averageWaitingTime +=servers.get(i).getTasks().get(k).getProcessingTime();
                    }
                }
            }
        }
    }

    public void printResults(PrintWriter printWriter,int currentTime){
        System.out.println("----------------------------------------------------------------");
        printWriter.println("-------------------------------------------------");
        textArea.appendText("----------------------------------------\n");
        System.out.println("Time: " + currentTime);
        printWriter.println("Time: " + currentTime);
        textArea.appendText("Time: " + currentTime +"\n");
        System.out.println("Waiting clients: ");
        printWriter.println("Waiting clients ");
        textArea.appendText("Waiting clients \n");
        for(int i=0;i<generatedTasks.size();i++){
            System.out.println("("+generatedTasks.get(i).getId()+","+generatedTasks.get(i).getArrivalTime()+"," +generatedTasks.get(i).getProcessingTime()+");");
            printWriter.println("("+generatedTasks.get(i).getId()+","+generatedTasks.get(i).getArrivalTime()+"," +generatedTasks.get(i).getProcessingTime()+");");
            textArea.appendText("("+generatedTasks.get(i).getId()+","+generatedTasks.get(i).getArrivalTime()+"," +generatedTasks.get(i).getProcessingTime()+");\n");
        }
        for(int i=0;i<servers.size();i++){
            System.out.println("Queue " + i +": ");
            printWriter.println("Queue " + i +": ");
            textArea.appendText("Queue " + i +": \n");
            if(servers.get(i).getTasks().isEmpty()){
                System.out.println("closed");
                printWriter.println("closed");
                textArea.appendText("closed\n");
            }else {
                for(int j=0;j<servers.get(i).getTasks().size();j++){
                    System.out.println("("+servers.get(i).getTasks().get(j).getId()+","+servers.get(i).getTasks().get(j).getArrivalTime()+"," +servers.get(i).getTasks().get(j).getProcessingTime()+");");
                    printWriter.println("("+servers.get(i).getTasks().get(j).getId()+","+servers.get(i).getTasks().get(j).getArrivalTime()+"," +servers.get(i).getTasks().get(j).getProcessingTime()+");");
                    textArea.appendText("("+servers.get(i).getTasks().get(j).getId()+","+servers.get(i).getTasks().get(j).getArrivalTime()+"," +servers.get(i).getTasks().get(j).getProcessingTime()+");\n");
                } } }
        computePeakHour(currentTime);
        computeAverageWaiting();
    }
    @Override
    public void run() {
        int currentTime = 0;
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        PrintWriter printWriter = null;
        try{ fileWriter = new FileWriter("Simulation.txt",false);
            bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
            printWriter.print(" ");
            printWriter.close();
            fileWriter = new FileWriter("Simulation.txt",true);
            bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
        }catch(IOException e){ }
        while(currentTime <= maxSimulation){
            prepareServers(currentTime);
            try {
                Thread.sleep(1000);
                //Platform.runLater(this::printResults(printWriter,currentTime));
                printResults(printWriter,currentTime);
                currentTime++;
                Thread.sleep(2000);
            }catch (InterruptedException e){ e.printStackTrace(); }
        }
        for(int i=0;i<servers.size();i++){ servers.get(i).setSimOver(true); }
        System.out.println("Average service time: " + (averageServiceTime/nrOfClients + averageWaitingTime/nrOfClients) + "\n" + "Peak hour is: " + peakHour + "\n" + "Average waiting time: " + averageWaitingTime/nrOfClients + "\n" + "The simulation is over");
        printWriter.println("Average service time: " + (averageServiceTime/nrOfClients + averageWaitingTime/nrOfClients) + "\n" + "Peak hour is: " + peakHour + "\n" + "Average waiting time: " + averageWaitingTime/nrOfClients + "\n" + "The simulation is over");
        try { printWriter.close();
            bufferedWriter.close();
            fileWriter.close();
        }catch(IOException e){ }
    }
}
