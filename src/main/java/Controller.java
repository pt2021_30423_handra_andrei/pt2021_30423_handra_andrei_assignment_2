import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller {


    public int nrOfClients;
    public int nrOfServers;
    public int maxSimulation;
    public int minArrival;
    public int maxArrival;
    public int minService;
    public int maxService;

    @FXML
    private TextField nrClients;
    @FXML
    private TextField nrQueues;
    @FXML
    private TextField simInterval;
    @FXML
    private TextField minArrivalTime;
    @FXML
    private TextField maxArrivalTime;
    @FXML
    private TextField minServiceTime;
    @FXML
    private TextField maxServiceTime;
    @FXML
    private Button submit;
    @FXML
    private TextField error;
    @FXML
    private Button reset;
    @FXML
    private TextArea textArea;

    @FXML
    public void onButtonClick(ActionEvent e){
        if(e.getSource().equals(submit)){
            if(nrQueues.getText().isEmpty() || nrClients.getText().isEmpty() || simInterval.getText().isEmpty()
                    || minArrivalTime.getText().isEmpty() || maxArrivalTime.getText().isEmpty() || minServiceTime.getText().isEmpty() || maxServiceTime.getText().isEmpty()){
                error.setText("Please complete all the fields");

            }else{
                try {
                    nrOfClients = Integer.parseInt(nrClients.getText());
                    nrOfServers = Integer.parseInt(nrQueues.getText());
                    maxSimulation = Integer.parseInt(simInterval.getText());
                    minArrival = Integer.parseInt(minArrivalTime.getText());
                    maxArrival = Integer.parseInt(maxArrivalTime.getText());
                    minService = Integer.parseInt(minServiceTime.getText());
                    maxService = Integer.parseInt(maxServiceTime.getText());
                    if(minArrival > maxArrival || minService > maxService){
                        throw new IllegalArgumentException();
                    }
                    SimulationManager simulationManager = new SimulationManager(nrOfClients, nrOfServers, maxSimulation, minArrival,
                            maxArrival, minService, maxService, textArea);
                    Thread t = new Thread(simulationManager);
                    t.start();
                }catch (NumberFormatException ex){
                    error.setText("Please enter valid number arguments");
                }catch (IllegalArgumentException ex){
                    error.setText("Please insert possible compute values");
                }

            }
        }else if(e.getSource().equals(reset)){
            error.clear();
            nrClients.clear();
            nrQueues.clear();
            simInterval.clear();
            minServiceTime.clear();
            maxServiceTime.clear();
            minArrivalTime.clear();
            maxArrivalTime.clear();
            textArea.setText(" ");
        }
    }
}
