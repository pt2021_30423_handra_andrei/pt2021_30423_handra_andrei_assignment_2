import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
    private List<Task> tasks;
    private AtomicInteger waitingPeriod;
    private boolean simOver;
    private String threadName;

    public boolean isSimOver() {
        return simOver;
    }

    public void setSimOver(boolean simOver) {
        this.simOver = simOver;
    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public void setWaitingPeriod(AtomicInteger waitingPeriod) {
        this.waitingPeriod = waitingPeriod;
    }

    public Server(String threadName){
        this.threadName = threadName;
        tasks = new ArrayList<>();
        waitingPeriod = new AtomicInteger();
        simOver = false;
    }

    public void addTask(Task newTask){
        tasks.add(newTask);
        waitingPeriod.addAndGet(newTask.getProcessingTime());
    }


    @Override
    public void run() {
        while(true){
            try{
                Thread.sleep(1000);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            if(!tasks.isEmpty()) {
                while (tasks.get(0).getProcessingTime() > 1) {
                    try {
                        Thread.sleep(1200);
                        tasks.get(0).setProcessingTime(tasks.get(0).getProcessingTime() - 1);
                        Thread.sleep(1200);
                        waitingPeriod.getAndDecrement();
                        Thread.sleep(1000);
                    } catch (InterruptedException e) { e.printStackTrace();}
                }
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){ e.printStackTrace(); }
                tasks.remove(0);
                waitingPeriod.getAndDecrement();
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){ e.printStackTrace(); }
            }
            if(simOver){ break; }
        }
    }
    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
